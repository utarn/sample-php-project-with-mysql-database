# Sample PHP website

Gitlab repo นี้เป็นตัวอย่างการสร้าง repo ที่มี Dockerfile พร้อมด้วยคำสั่งสำหรับ build image
ข้อความที่นักศึกษาเห็นตรงนี้เป็นภาษา Markdown
ลองใช้ notepad หรือ VS Code เปิดไฟล์ README.md ดูสิครับ

# การสร้างอิมเมจ

- จะ Push ได้สร้าง private key ก่อน เปิด powershell หรือ cmd จากนั้นพิมพ์

```
ssh-keygen
```

![Image of ssh-keygen](https://gitlab.com/utarn/sample-php-project-with-mysql-database/-/design_management/designs/102073/b152d5cc0141b66eac3675868755089a185b5e1c/raw_image)

เราจะได้ไฟล์ id_rsa ที่โฟดเดอร์ C:\Users\<ชื่อผู้ใช้>\.ssh
เอาไฟล์ id_rsa ไปเปิดด้วยโปรแกรม PuttyGen จากนั้นคัดลอกตรงกล่องข้อความ Public key for pasting....

![Image of Puttygen](https://gitlab.com/utarn/sample-php-project-with-mysql-database/uploads/3ba81cedec4cacda0731b13ddda1aa79/puttygen_XCAdiOjcPk.png)

- ไปเว็บ Gitlab ล็อกอินให้เรียบ้อย > ต้องไป Settings มุมขวาบนเลือก SSH Keys และวาง Public key จาก Putty gen ลงไป

![Image of Gitlab SSH key](https://gitlab.com/utarn/sample-php-project-with-mysql-database/-/design_management/designs/102076/389e1d350df9a33389faf5b6171a2521e4dc80f1/raw_image)

- ใครอยากไปต่อใช้ Gitlab สร้าง image ให้เราฟรี พร้อมที่เก็บ นักศึกษาสามารถตรวจสอบแฟ้มข้อมูล .gitlab-ci.yml ไฟล์นี้สร้างหรือไม่ก็ได้ แต่ถ้าสร้างมันจะ build docker image ให้เราอัตโนมัติเลยครับ
- จากนั้นเปลี่ยน registry เป็นชื่อ url ของ project ที่สร้างใน gitlab หรือใครจะให้ manual ใช้ image จาก Docker hub ที่ลองบิวด์เองว่าผ่านแล้วดูก็ได้ครับ

### ชื่อ mysql ของ database มันจะตรงกับโค้ดใน myweb ใช้คำว่า localhost ไม่ได้นะครับ

เขียนคำสั่งแบบนี้ไว้ใน repo ให้อาจารย์ด้วยครับ ปรับเปลี่ยนเป็นของตนเอง

```sh
 $ docker build -t registry.gitlab.com/utarn/sample-php-project-with-mysql-database .
 $ docker network create mynetwork
 $ docker run -d --name myweb --network mynetwork --restart always -p 80:80 \
   registry.gitlab.com/utarn/sample-php-project-with-mysql-database
 $ docker run -d --name mysql --network mynetwork --restart always \
   -e MYSQL_ROOT_PASSWORD=password -e MYSQL_DATABASE=mydatabase \
   bitnami/mysql:5.7

```

# อยากลองทำดู

- mysql มีอิมเมจทั้ง mysql:8.0 ,bitnami/mysql และอื่นๆอีกมากมายครับ
- โปรเจคภาษาอื่นต่างกันตรงที่ Dockerfile ว่าจะเขียนยังไง กับคำสั่ง docker run ของ database
- ลองสำรวจที่เมนู CI/CD > Pipelines > Jobs ดูครับ จะเป็นหน้าจอที่ gitlab สร้างอิมเมจให้เราอัตโนมัติ
- จะใช้ตัวแปร \$CI_JOB_TOKEN ใน .gitlab-ci.yml ได้ก็ต่อเมื่อ repo นี้เป็นสาธารณะครับ
- ตรง Project overview จะมีปุ่ม Fork อยู่ขวามือ สามารถ Fork ไปเป็นของตัวเองแล้วรันดูได้ครับ
- ตรง Packages & Registries > Container Registry นักศึกษาจะเห็น Image repositories ที่ตัว gitlab สร้างให้เราอัตโนมัติครับ สามารถคัดลอกไปใช้งานได้

[ดูรายละเอียดการเขียน Markdown ได้ที่](https://dillinger.io/)
